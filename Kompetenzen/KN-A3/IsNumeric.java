package ch.tbz;

public class IsNumeric
{
    /**
     * Returns {@code true} if the String contains only Unicode digits.
     * An empty string or {@code null} leads to {@code false}.
     *
     * @param  string Input String.
     * @return {@code true} if string is numeric, {@code false} otherwise.
     */
    public static boolean isNumeric( String string )
    {
        if ( string == null || string.length() == 0 )
            return false;

        for ( int i = 0; i < string.length(); i++ )
            if ( ! Character.isDigit( string.charAt( i ) ) )
                return false;

        return true;
    }

    public static void main( String[] args )
    {
        String test = "1234"; // "12.4" --> false // "-123" --> false
        boolean hasOnlyDigits = isNumeric( test );
        System.out.println( hasOnlyDigits );  // "1234" --> true
    }
}